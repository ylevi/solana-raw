# Encodings
import base64
import based58
# Solana
from solana.rpc.core import RPCException, RPCNoResultException, UnconfirmedTxError
from solana.rpc.api import Client, PublicKey, Transaction
from solana.transaction import TransactionInstruction, NonceInformation, Blockhash
from solana.system_program import create_account, CreateAccountParams, nonce_advance, AdvanceNonceParams, \
    SYS_PROGRAM_ID, nonce_initialization, InitializeNonceParams
from solana.keypair import Keypair
from solders.signature import Signature
# Solana Token Program
import spl.token.instructions as tokenization
from spl.token.constants import TOKEN_PROGRAM_ID
# Fireblocks & Utils
from utils.helpers import *
from fireblocks_sdk import FireblocksSDK, TransferPeerPath, VAULT_ACCOUNT, RawMessage, UnsignedMessage, \
    TRANSACTION_STATUS_COMPLETED, TRANSACTION_STATUS_FAILED, TRANSACTION_STATUS_BLOCKED, TRANSACTION_STATUS_CANCELLED
import time
import os

# SDK Initialization
API_KEY = os.environ['TEST_API_KEY']
API_SECRET = os.environ['SECRET']
FBKS = FireblocksSDK(API_SECRET, API_KEY)

# Durable transaction configuration, NFT configuration.
DURABLE_NONCE = 1  # 0 for regular blockhash usage (non-durable nonce), 1 for durable nonce.
DECIMALS = 0  # 0 for NFT, else for FT


class RawSolana:
    _chain_to_asset = {
        SolanaChain.MAIN: ('SOL', 'https://api.mainnet-beta.solana.com'),
        SolanaChain.DEV: ('SOL_TEST', 'https://api.devnet.solana.com'),
        SolanaChain.TEST: ('SOL_TEST', 'https://api.testnet.solana.com')
    }

    def __init__(self, sdk: FireblocksSDK, chain: SolanaChain, vault_id: str):
        self.sdk = sdk
        self.vault_id = vault_id
        self.chain = chain
        self.asset = self._chain_to_asset[chain][0]
        self.client = Client(self._chain_to_asset[chain][1])
        self.pub_key = PublicKey(self.sdk.get_deposit_addresses(self.vault_id, self.asset)[0][ADDRESS])
        self.mint_pub_key = PublicKey(self.retrieve_derived_address(DERIVATION_CHANGE, DERIVATION_INDEX))
        self.nonce_pub_key = PublicKey(self.retrieve_derived_address(DERIVATION_CHANGE + 1, DERIVATION_INDEX + 1))

    def __str__(self):
        return f"RawSolana Client on asset [{self.asset}] | Address [{self.pub_key.__str__()}]"

    @property
    def chain_to_asset(self):
        return self._chain_to_asset

    @chain_to_asset.setter
    def chain_to_asset(self, chain_asset):
        pass

    # Helpers

    @staticmethod
    def create_keypair():
        """
        :return: The hex representation of the secret key.
        """
        keypair = Keypair()
        return keypair.secret_key.hex()

    # Fireblocks Functions

    def retrieve_derived_address(self, derivation_change: int, derivation_index: int) -> bytes:
        """
        :param derivation_change: Change for address derivation (see bip-32)
        :param derivation_index: Index for address derivation (see bip-32)
        :return: The byte representation of the PublicKey
        """
        new_address = self.sdk.get_public_key_info_for_vault_account(self.asset,
                                                                     self.vault_id,
                                                                     derivation_change,
                                                                     derivation_index,
                                                                     True)[PUB_KEY]

        return bytes.fromhex(new_address)

    def check_tx_is_completed(self, tx_id) -> dict:
        """
        This function waits for SUBMIT_TIMEOUT*4 (180 by default) seconds to retrieve status of the transaction sent to
        Fireblocks. Will stop upon completion / failure.
        :param tx_id: Transaction ID from FBKS.
        :return: Transaction last status after timeout / completion.
        """
        timeout = 0
        current_status = self.sdk.get_transaction_by_id(tx_id)[STATUS_KEY]
        while current_status not in (
                TRANSACTION_STATUS_COMPLETED, TRANSACTION_STATUS_FAILED, TRANSACTION_STATUS_BLOCKED,
                TRANSACTION_STATUS_CANCELLED) and timeout < SUBMIT_TIMEOUT:
            print(f"TX [{tx_id}] is currently at status - {current_status} {'.' * (timeout % 4)}                ",
                  end="\r")
            time.sleep(4)
            current_status = self.sdk.get_transaction_by_id(tx_id)[STATUS_KEY]
            timeout += 1

        print(f"\nTX [{tx_id}] is currently at status - {current_status}")
        return current_status

    def retrieve_signatures(self, tx_id: str) -> list:
        """
        Attempts to retrieve signature, otherwise raises error.
        :param tx_id:  Transaction ID from FBKS.
        :return: Returns FBKS "fullSig", to be pushed to Stellar TransactionEnvelope.
        """
        final_status = self.check_tx_is_completed(tx_id)
        if final_status == TRANSACTION_STATUS_COMPLETED:
            messages = self.sdk.get_transaction_by_id(tx_id)[MSG_KEY]
            return [{PUB_KEY: message[PUB_KEY], FULL_SIG_KEY: message[SIG_KEY][FULL_SIG_KEY]} for message in messages]
        else:
            if final_status == TRANSACTION_STATUS_FAILED:
                raise ValueError("Transaction resulted with status FAILED. Verify you have a valid transaction built.")
            elif final_status == TRANSACTION_STATUS_BLOCKED:
                raise NotImplementedError("Transaction Authorization Policy not implemented or Raw Signing disabled.")
            else:
                raise TimeoutError("Couldn't retrieve DONE status in a timely fashion.")

    def sign_transaction(self, transaction: Transaction,
                         authority_sign: bool,
                         derivation_signers: list[tuple[int, int]],
                         vault_signers: list[int] = None):
        """
        :param transaction: A solana.rpc.api Transaction object.
        :param authority_sign: Whether the mint_authority account should sign (the vault that was used to create it).
        :param derivation_signers: Whether the token account should sign (usually used only for CreateAccount)
        :param vault_signers: A list of ints representing the Ids of vaults that also need to sign the transaction.
        :return: The transaction, signed.
        """
        serialized_msg = transaction.serialize_message()
        hexed_serialized_msg = serialized_msg.hex()
        message_list, tx_signatures = [], []
        if authority_sign:
            unsigned_message = UnsignedMessage(hexed_serialized_msg)
            message_list.append(unsigned_message)

        if derivation_signers:
            for derivation_signer in derivation_signers:
                derivation_index = derivation_signer[0]
                derivation_change = derivation_signer[1]
                minter_unsigned_message = UnsignedMessage(hexed_serialized_msg, derivation_index, derivation_change)
                message_list.append(minter_unsigned_message)

        if message_list:
            raw_messages = RawMessage(message_list)
            tx_id = self.sdk.create_raw_transaction(raw_messages,
                                                    TransferPeerPath(VAULT_ACCOUNT, self.vault_id),
                                                    self.asset)[ID_KEY]
            tx_signatures = self.retrieve_signatures(tx_id)
            for current_signature_item in tx_signatures:
                current_key = PublicKey(bytes.fromhex(current_signature_item[PUB_KEY]))
                signature = Signature(bytes.fromhex(current_signature_item[FULL_SIG_KEY]))
                transaction.add_signature(current_key, signature)

        if vault_signers:
            vault_signers = [str(item) for item in vault_signers]
            for vault in vault_signers:
                vault_unsigned_message = UnsignedMessage(hexed_serialized_msg)
                vault_raw_message = RawMessage([vault_unsigned_message])
                vault_tx_id = self.sdk.create_raw_transaction(vault_raw_message,
                                                              TransferPeerPath(VAULT_ACCOUNT, vault),
                                                              self.asset)[ID_KEY]
                tx_signature = self.retrieve_signatures(vault_tx_id)[0]
                tx_signatures.append(tx_signature)
                vault_public_key = PublicKey(bytes.fromhex(tx_signature[PUB_KEY]))
                tx_final_signature = Signature(bytes.fromhex(tx_signature[FULL_SIG_KEY]))
                transaction.add_signature(vault_public_key, tx_final_signature)

        return tx_signatures

    # Solana Functions
    # Instructions
    def _create_nonce_account_instruction(self) -> TransactionInstruction:
        """
        :return: Instruction to create a nonce account.
        """
        return create_account(
            CreateAccountParams(
                from_pubkey=self.pub_key,
                new_account_pubkey=self.nonce_pub_key,
                program_id=SYS_PROGRAM_ID,
                space=ACCOUNT_LAYOUT.sizeof(),
                lamports=self.client.get_minimum_balance_for_rent_exemption(ACCOUNT_LAYOUT.sizeof())['result']
            )
        )

    def _create_nonce_initialization_instruction(self) -> TransactionInstruction:
        """
        :return: Instruction to initialize a created nonce account.
        """
        return nonce_initialization(
            InitializeNonceParams(
                nonce_pubkey=self.nonce_pub_key,
                authorized_pubkey=self.pub_key
            )
        )

    def _advance_nonce_instruction(self) -> TransactionInstruction:
        """
        :return: Instruction to advance the nonce of the Nonce account.
        """
        return nonce_advance(
            AdvanceNonceParams(
                nonce_pubkey=self.nonce_pub_key,
                authorized_pubkey=self.pub_key
            )
        )

    def _create_token_account_instruction(self) -> TransactionInstruction:
        """
        :return: Instruction to create a token account (account under the Solana token program).
        """
        return create_account(
            CreateAccountParams(
                from_pubkey=self.pub_key,
                program_id=TOKEN_PROGRAM_ID,
                new_account_pubkey=self.mint_pub_key,
                space=MINT_LAYOUT.sizeof(),
                lamports=self.client.get_minimum_balance_for_rent_exemption(MINT_LAYOUT.sizeof())['result']
            )
        )

    def _create_mint_instruction(self) -> TransactionInstruction:
        """
        :return: Creating a token account (mint). Recommended to use with the create_token_account instruction.
        """
        return tokenization.initialize_mint(
            tokenization.InitializeMintParams(
                program_id=TOKEN_PROGRAM_ID,
                mint=self.mint_pub_key,
                mint_authority=self.pub_key,
                freeze_authority=self.pub_key,
                decimals=DECIMALS
            )
        )

    def _create_associated_token_account_instruction(self, token_receiver: str | PublicKey) -> TransactionInstruction:
        """
        Creates an ATA (associated token account, meaning wallet address + mint address) for given address.
        :param token_receiver: Address to receive the token.
        :return: ATA Instruction.
        """
        if isinstance(token_receiver, str):
            token_receiver = PublicKey(token_receiver)
        return tokenization.create_associated_token_account(
            self.pub_key,
            token_receiver,
            self.mint_pub_key
        )

    def _create_mint_to_instruction(self, receiver_address: PublicKey, amount: int) -> TransactionInstruction:
        """
        :param receiver_address: Receiver address which is an associated token account
        :return: Instruction to mint a certain amount of tokens to an address.
        """
        return tokenization.mint_to(
            tokenization.MintToParams(
                program_id=TOKEN_PROGRAM_ID,
                mint=self.mint_pub_key,
                dest=receiver_address,
                mint_authority=self.pub_key,
                amount=amount,
                signers=[self.pub_key]
            )
        )

    def _create_burn_instruction(self, token_owner: str | PublicKey, amount: int) -> TransactionInstruction:
        """
        :param token_owner: The owner of the associated token account. This should be just the public address of the
        owner, not the associated token account address.
        :param amount: An amount of tokens to burn.
        :return: A instruction to burn a specific amount of tokens.
        """
        if isinstance(token_owner, str):
            token_owner = PublicKey(token_owner)
        associated_account = tokenization.get_associated_token_address(token_owner, self.mint_pub_key)
        if not self.client.get_account_info(associated_account)['result']['value']:
            raise ValueError(f"Target does not have an associated token account, meaning it also has no tokens.")
        return tokenization.burn(
            tokenization.BurnParams(
                program_id=TOKEN_PROGRAM_ID,
                account=associated_account,
                mint=self.mint_pub_key,
                owner=token_owner,
                amount=amount
            )
        )

    # Transaction Broadcasting
    def create_solana_transaction(self, instructions: list[TransactionInstruction], is_durable: int) -> Transaction:
        """
        :param instructions: A list of instructions for the transaction.
        :param is_durable: If using Nonce Account (1) for a Durable Transaction, or not (0).
        :return: A solana Transaction object.
        """
        transaction = Transaction(fee_payer=self.pub_key)
        if is_durable:
            transaction.add(self._advance_nonce_instruction())
            nonce_account_info = self.client.get_account_info(self.nonce_pub_key)
            b64_nonce = nonce_account_info['result']['value']['data'][0]
            bytes_array_nonce = bytearray(base64.b64decode(b64_nonce))[40:72]
            nonce = based58.b58encode(bytes(bytes_array_nonce)).decode()
            transaction.nonce_info = NonceInformation(
                nonce=Blockhash(nonce),
                nonce_instruction=self._advance_nonce_instruction()
            )
            transaction.recent_blockhash = nonce
        else:
            transaction.recent_blockhash = self.client.get_recent_blockhash()['result']['value']['blockhash']

        for instruction in instructions:
            transaction.add(instruction)

        return transaction

    def submit_transaction(self, instructions: list[TransactionInstruction],
                           authority_sign: bool = True,
                           derivation_signers: list[tuple[int, int]] = None,
                           vault_signers: list[int] = None,
                           is_durable: int = DURABLE_NONCE,
                           fee_payer: PublicKey = None):
        """
        :param instructions: A list of instructions for the transaction.
        :param authority_sign: True by default. This mean the vault wallet will sign the transaction.
        :param derivation_signers: A derivation account of the same vault, basically needed for token account / nonce
        account.
        :param vault_signers: If other vaults might need to sign the transaction.
        :param is_durable: If using Nonce Account for a Durable Transaction.
        :param fee_payer: If a different account will pay for the fee (and sign thus, sign the transaction). By default,
        it is the vault asset wallet by default.
        :return: Prints the link to the transaction or the relevant error.
        """
        transaction = self.create_solana_transaction(instructions, is_durable)
        if fee_payer:
            transaction.fee_payer = fee_payer
        self.sign_transaction(transaction, authority_sign, derivation_signers, vault_signers)
        print("Attempting to broadcast transaction ...")
        try:
            resp = self.client.send_raw_transaction(transaction.serialize())
            url = f"https://explorer.solana.com/tx/{resp['result']}"
            if self.chain == SolanaChain.DEV or self.chain == SolanaChain.TEST:
                url += f"?cluster={self.chain.value}"

            print(f"You may view your transaction at the following URL:\n{url}")
            return resp
        except RPCException as exception:
            print(f"Exception occurred during RPC request / simulation, please view the failure message:\n{exception}")
        except RPCNoResultException as exception:
            print(f"No result from RPC request, please view the failure message:\n{exception}")
        except UnconfirmedTxError as exception:
            print(f"Could not complete the transaction in a timely fashion, please view the failure message:"
                  f"\n{exception}")

    def create_token_account(self) -> dict:
        """
        Submits a token creation transaction after signing it with Fireblocks.
        :return: Response if no exceptions were raised
        """
        instruction = self._create_token_account_instruction()
        return self.submit_transaction([instruction], derivation_signers=[(DERIVATION_INDEX, DERIVATION_CHANGE)])

    def create_nonce_account(self) -> dict:
        """
        Submits a nonce account creation transaction after signing it with Fireblocks.
        :return: Response if no exceptions were raised
        """
        account_instruction = self._create_nonce_account_instruction()
        nonce_initialization_instruction = self._create_nonce_initialization_instruction()
        return self.submit_transaction([account_instruction, nonce_initialization_instruction],
                                       derivation_signers=[(DERIVATION_INDEX + 1, DERIVATION_CHANGE + 1)],
                                       is_durable=0)

    def create_required_accounts(self) -> dict:
        """
        Submits a transaction that creates both token account, nonce account and initializes nonce account as well
        (only, mint account is only created, not initialized).
        :return: Response if no exceptions were raised
        """
        token_account_instruction = self._create_token_account_instruction()
        nonce_account_instruction = self._create_nonce_account_instruction()
        nonce_initialization_instruction = self._create_nonce_initialization_instruction()
        return self.submit_transaction(instructions=[token_account_instruction,
                                                     nonce_account_instruction,
                                                     nonce_initialization_instruction],
                                       derivation_signers=[
                                           (DERIVATION_INDEX, DERIVATION_CHANGE),
                                           (DERIVATION_INDEX + 1, DERIVATION_CHANGE + 1)
                                       ],
                                       is_durable=0)

    def create_mint(self, initialize_account: bool = False) -> dict:
        """
        This function creates a token (aka token mint account)
        :param initialize_account: False by default. If you would like to create a single transaction that both
        creates the account and initializes the mint set to True.
        :return: Response if no exceptions were raised
        """
        instruction_list = []
        additional_signers = []
        if initialize_account:
            create_instruction = self._create_token_account_instruction()
            instruction_list.append(create_instruction)
            additional_signers.append((DERIVATION_INDEX, DERIVATION_CHANGE))
        mint_instruction = self._create_mint_instruction()
        instruction_list.append(mint_instruction)
        return self.submit_transaction(instruction_list, derivation_signers=additional_signers)

    def create_associated_token_account(self, token_receiver: str | PublicKey) -> dict:
        """
        :param token_receiver: The address that will receive the token, and have an ATA created for it.
        :return: Response if no exceptions were raised
        """
        instruction = self._create_associated_token_account_instruction(token_receiver)
        return self.submit_transaction([instruction])

    def mint_to(self, receiver_address: str | PublicKey, amount: int) -> dict:
        """
        This function attempts to send a token to a specific address (not an ATA), and if it doesn't have an ATA it will
        create one for the destination account.
        :param receiver_address: The address of the wallet to receive the token (not its ATA).
        :param amount: Amount of tokens to send.
        :return: Response if no exceptions were raised
        """
        if isinstance(receiver_address, str):
            receiver_address = PublicKey(receiver_address)
        associated_token_address = tokenization.get_associated_token_address(receiver_address,
                                                                             self.mint_pub_key)
        if not self.client.get_account_info(associated_token_address)['result']['value']:
            setup_associated_account = self._create_associated_token_account_instruction(receiver_address)
            print("Receiver doesn't have an associated token account set up yet.\nSetting it up now...")
            account_setup = self.submit_transaction([setup_associated_account])
            print(f"Setup result:\n{account_setup}")
            print("Sleeping for 10 seconds to make sure transaction has been broadcast, before minting.")
            time.sleep(10)
            print("Attempting mint transaction.")
        else:
            print("Receiver has an associated token address, attempting to mint.")
        instruction = self._create_mint_to_instruction(associated_token_address, amount)
        return self.submit_transaction([instruction])

    def burn_from(self, vault_id: int, amount: int) -> dict:
        """
        The amount to burn from a certain vault.
        :param vault_id: ID of the vault.
        :param amount: Amount of tokens to burn.
        :return: Response if no exceptions were raised
        """
        token_owner = PublicKey(self.sdk.get_deposit_addresses(str(vault_id), self.asset)[0][ADDRESS])
        instruction = self._create_burn_instruction(token_owner, amount)
        return self.submit_transaction([instruction], vault_signers=[vault_id])


if __name__ == "__main__":
    dev_chain = SolanaChain.DEV
    raw_solana = RawSolana(FBKS, dev_chain, "0")
