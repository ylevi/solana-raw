from enum import Enum
from construct import Int8ul, Int32ul, Int64ul, Bytes, Struct as cStruct

# DO NOT EDIT
ADDRESS = "address"
SUBMIT_TIMEOUT = 45
STATUS_KEY = "status"
SIG_KEY = "signature"
FULL_SIG_KEY = "fullSig"
MSG_KEY = "signedMessages"
ID_KEY = "id"
PUB_KEY = "publicKey"
BYTE_START, BYTE_END = 40, 72
DERIVATION_CHANGE = DERIVATION_INDEX = 3
# DO NOT EDIT


class SolanaChain(Enum):
    MAIN = "mainnet"
    DEV = "devnet"
    TEST = "testnet"


# Layouts
PUBLIC_KEY_LAYOUT = Bytes(32)
MINT_LAYOUT = cStruct(
    "mint_authority_option" / Int32ul,
    "mint_authority" / PUBLIC_KEY_LAYOUT,
    "supply" / Int64ul,
    "decimals" / Int8ul,
    "is_initialized" / Int8ul,
    "freeze_authority_option" / Int32ul,
    "freeze_authority" / PUBLIC_KEY_LAYOUT,
)

NONCE_ACCOUNT_LAYOUT = cStruct(
    "authorizedPubkey" / Int8ul,
    "feeCalculator" / Int64ul,
    "nonce" / Int8ul,
    "state" / Int32ul,
    "version" / Int32ul,
    "buffer1" / Int64ul,
    "buffer2" / Int64ul
)

ACCOUNT_LAYOUT = cStruct(
    "mint" / PUBLIC_KEY_LAYOUT,
    "owner" / PUBLIC_KEY_LAYOUT,
    "amount" / Int64ul,
    "delegate_option" / Int32ul,
    "delegate" / PUBLIC_KEY_LAYOUT,
    "state" / Int8ul,
    "is_native_option" / Int32ul,
    "is_native" / Int64ul,
    "delegated_amount" / Int64ul,
    "close_authority_option" / Int32ul,
    "close_authority" / PUBLIC_KEY_LAYOUT,
)